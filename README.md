#### IndexedDB
##### 介绍
浏览器数据库 IndexedDB IndexedDB 就是浏览器提供的本地数据库，它可以被网页脚本创建和操作。IndexedDB 允许储存大量数据，提供查找接口，还能建立索引。这些都是 LocalStorage 所不具备的。就数据库类型而言，IndexedDB 不属于关系型数据库（不支持 SQL 查询语句），更接近 NoSQL 数据库。 IndexedDB 是一个比较复杂的 API，涉及不少概念。它把不同的实体，抽象成一个个对象接口。学习这个 API，就是学习它的各种对象接口。

数据库：IDBDatabase 对象 对象仓库：IDBObjectStore 对象 索引： IDBIndex 对象 事务： IDBTransaction 对象 操作请求：IDBRequest 对象 指针： IDBCursor 对象 主键集合：IDBKeyRange 对象 下面是一些主要的概念。

（1）数据库

数据库是一系列相关数据的容器。每个域名（严格的说，是协议 + 域名 + 端口）都可以新建任意多个数据库。

IndexedDB 数据库有版本的概念。同一个时刻，只能有一个版本的数据库存在。如果要修改数据库结构（新增或删除表、索引或者主键），只能通过升级数据库版本完成。

（2）对象仓库

每个数据库包含若干个对象仓库（object store）。它类似于关系型数据库的表格。

（3）数据记录

对象仓库保存的是数据记录。每条记录类似于关系型数据库的行，但是只有主键和数据体两部分。主键用来建立默认的索引，必须是不同的，否则会报错。主键可以是数据记录里面的一个属性，也可以指定为一个递增的整数编号。

上面的对象中，id属性可以当作主键。

数据体可以是任意数据类型，不限于对象。

（4）索引

为了加速数据的检索，可以在对象仓库里面，为不同的属性建立索引。

（5）事务

数据记录的读写和删改，都要通过事务完成。事务对象提供error、abort和complete三个事件，用来监听操作结果。
#### 使用方法
```JS
npm 安装
npm i yiu-indexeddb
```
##### 创建DB new DB();

```js
let db = new DB('数据库名称', '表名称');
// 例
let db = new DB('web_DB', 'nav_text');
```

##### 数据存储 db.save()

```js
db.save('存储唯一Key','数据')
// 例
let data = {name:'管理员', roleId: 1, type: 1};
db.save('list',data).then((res)=>{
	console.log(res,'存储成功')
})
```

##### 查询 db.get()

```js
db.get('存储唯一Key')
// 例
db.get('list').then((res)=>{
	console.log(res,'查询成功')
})
```

##### 删除 db.delete()

```js
db.delete('存储唯一Key')
// 例
db.delete('list').then(res=>{
    console.log(res,'删除成功---->>>>>>name')
})
```

##### 清除数据库 db.deleteAll()

```js
db.deleteAll()
// 例
db.deleteAll().then(res=>{
	console.log(res,'清除数据库---->>>>>>name')
})
```



#### 错误码

name|code|describe
:--:|:--:|:--:|
success|200|成功
error|401|key不存在
open|91001|打开数据库失败的错误
save|91002|保存数据失败的错误
get|91003|获取数据失败的错误
delete|91004|删除数据失败的错误
deleteAll|91005|清空数据库失败的错误

#### Vue3 使用方法

##### 全局注册

```js
main.js

import dbClass from '@/utils/dbClass.js';
const app = createApp(App);
app.config.globalProperties.$DB = dbClass;
```

##### 使用

````js
setup() {
    const { proxy } = getCurrentInstance();
    let data = { theme: 'iYiu', title: 'IndexedDB' }; //将被存的数据，也可以字符串
    let key = 'oneNewData'; //存数据的key值
    let DB = new proxy.$DB('web_table','test');
    DB.save(key, data).then(res=>{
        console.log(res)
    })
    DB.get('oneNewData').then(res=>{
        console.log(res)
    })
}
````

